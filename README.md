# TTM00-FW-PIC32-Training-V1

1. Project Brief: Practice of control version software Git using command window and SourceTree.
2. Hardware description:
* PIC32MX470512H: https://ww1.microchip.com/downloads/en/DeviceDoc/PIC32MX330350370430450470_Datasheet_DS60001185H.pdf
* Curiosity board user guide: https://ww1.microchip.com/downloads/en/DeviceDoc/70005283B.pdf
3. Serial commands.
4. Prerequisites
    1. SDK Version:
        * IDE version: MPLAB X IPE v5.50
        * Compiler version: v1.42
        * Project configuration
            * Microchip Embedded 
            * Projects: Standalone project
            * Family: All families
            * Device: PIC32MX470F512H
            * Hardware tools: Microchip start
            * Compiler: XC32(v1.42)
            * Encoding: ISO-8859-1
5. Versioning
     1. Current version of the FW
        * V1.0. 20210713
6. Authors
    1. Project staff: Alejandro Ramírez Jaramillo
    2. Maintainer contact email: alramirezja@unal.edu.co



