/* 
 * File:   main.c
 * Author: Alejandro Ramirez
 *
 * Created on 13 de julio de 2021, 09:55 AM
 */
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "plib.h"
#include "main.h"
#include "config_bits.h"



uint32_t  count=0;
uint32_t  count_g=0;
uint32_t  count_y=0;
uint32_t  heart_b=0;
char buffer[4];
uint32_t index=0; 
uint32_t  period_g;
uint32_t  period_y;

void gpio_init(void);
void button_polling(void);

void main (void) {
    
    gpio_init();
    //timer1_init();
    //uart1_init();
    while (1){
        button_polling();
      //  timer1_polling_flag();
      //  uart1_tx();
      //  uart1_rx_polling();
      //  gpio_uart(period_g, period_y);
    }    
}
void gpio_init(void){
    //Yellow LED as output LED3
    TRISBbits.TRISB10 = 0;
    //Green LED as output LED2
    TRISBbits.TRISB3 = 0;
    //red LED as output LED1
    TRISBbits.TRISB2 = 0;
    
    TRISEbits.TRISE4 = 0;
    TRISEbits.TRISE6 = 0;
    TRISEbits.TRISE7 = 0;
    //s1 as input 
    TRISDbits.TRISD6 = 1;
}


void button_polling(void){
    if (PORTDbits.RD6 == 0) {
        LATBbits.LATB10 = 1;
        LATBbits.LATB3 = 0;
        LATBbits.LATB2 = 0;
    }
    else {
        LATBbits.LATB10 = 0;
        LATBbits.LATB3 = 0;
        LATBbits.LATB2 = 0;
    }
}
